package com.evry.statemachine.contentparsers;


import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.Utils.StatemachineUtils;

public class XmlContentParser {

    public static XmlContent getXmlContentFromString(String xmlContent) throws Exception {
            InputStream xmlStream = new ByteArrayInputStream(xmlContent.getBytes());

            String xsdFilePath = StatemachineUtils.getAbsoluteFilePath("Datastructure.xsd");
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(xsdFilePath));

            JAXBContext jaxbContext = JAXBContext.newInstance(XmlContent.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);
            XmlContent content = (XmlContent) unmarshaller.unmarshal(xmlStream);
            return content;
    }

    public static FileDataHolder getFileDataFromXmlContent(XmlContent xmlContent) {
        return FileDataHolder.with()
                .company(xmlContent.getCompany())
                .startedBy(xmlContent.getStartedBy())
                .startedDate(xmlContent.getStartedDate())
                .build();
    }
}
