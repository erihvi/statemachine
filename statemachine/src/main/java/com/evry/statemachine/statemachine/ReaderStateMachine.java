package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.common.StateMachineBase;

public class ReaderStateMachine extends StateMachineBase<ReaderStateEnum, ReaderStateEntityBase> {

    private final StateFault stateFault;
    private final StateInitial stateInitial;
    private final StateInvalid stateInvalid;
    private final StateParseFlatFile stateParseFlatFile;
    private final StateParseXml stateParseXml;
    private final  StateReadFile stateReadFile;
    private final StateValid stateValid;
    private final StateValidating stateValidating;

    private String filePath = null;
    private String fileContent = null;
    private FileDataHolder fileDataHolder = null;

    public ReaderStateMachine() {
        stateFault = new StateFault(this);
        stateInitial = new StateInitial(this);
        stateInvalid = new StateInvalid(this);
        stateParseFlatFile = new StateParseFlatFile(this);
        stateParseXml = new StateParseXml(this);
        stateReadFile = new StateReadFile(this);
        stateValid = new StateValid(this);
        stateValidating = new StateValidating(this);
        currentState = stateInitial;
    }

    @Override
    public void init() {
        currentState = stateInitial;
    }

    @Override
    public ReaderStateEnum getCurrentState() {
        return currentState.getStateEnum();
    }

    @Override
    public void setState(ReaderStateEnum state) {
        previousState = currentState;

        switch (state) {
            case INITIAL_STATE:
                currentState = stateInitial;
                break;
            case READ_FILE_STATE:
                currentState = stateReadFile;
                break;
            case FAULT_STATE:
                currentState = stateFault;
                break;
            case PARSE_XML_STATE:
                currentState = stateParseXml;
                break;
            case PARSE_FLAT_FILE_STATE:
                currentState = stateParseFlatFile;
                break;
            case VALID_STATE:
                currentState = stateValid;
                break;
            case INVALID_STATE:
                currentState = stateInvalid;
                break;
            case VALIDATING_STATE:
                currentState = stateValidating;
                break;
            default:
                currentState = stateFault;
                break;
        }

        changeState();
    }

    @Override
    public void handle() throws Exception {
        try {
            super.handle();
        } catch (Exception e) {
            setState(ReaderStateEnum.FAULT_STATE);
            stateFault.setException(e);
            super.handle();
        }
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public FileDataHolder getFileDataHolder() {
        return fileDataHolder;
    }

    public void setFileDataHolder(FileDataHolder fileDataHolder) {
        this.fileDataHolder = fileDataHolder;
    }
}
