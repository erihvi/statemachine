package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class StateValidating extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateValidating.class);

    private FileDataHolder fileDataHolder = null;

    public StateValidating(ReaderStateMachine parent) {
        super(ReaderStateEnum.VALIDATING_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateValidating");
        fileDataHolder = parent.getFileDataHolder();
    }

    @Override
    public void handle() throws Exception {
        boolean valid = true;

        if (!fileDataHolder.getCompany().matches("[A-Za-z]+")) {
            valid = false;
            LOGGER.warn("invalid company: {}", fileDataHolder.getCompany());
        }

        if (!fileDataHolder.getStartedBy().matches("[A-Za-z]+")) {
            valid = false;
            LOGGER.warn("invalid startedBy: {}", fileDataHolder.getStartedBy());
        }


        LocalDate validationDate = LocalDate.parse("2008-09-29");
        if (!fileDataHolder.getStartedDate().equals(validationDate)) {
            valid = false;
            LOGGER.warn("invalid startedDate: {}", fileDataHolder.getStartedDate().toString());
        }

        if (valid){
            parent.setState(ReaderStateEnum.VALID_STATE);
        } else{
            parent.setState(ReaderStateEnum.INVALID_STATE);
        }
        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateValidating");
        fileDataHolder = null;
    }
}
