package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.contentparsers.XmlContent;
import com.evry.statemachine.contentparsers.XmlContentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateParseXml extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateParseXml.class);

    public StateParseXml(ReaderStateMachine parent) {
        super(ReaderStateEnum.PARSE_XML_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateParseXml");
    }

    @Override
    public void handle() throws Exception {
        String content = parent.getFileContent();
        XmlContent xmlContent = XmlContentParser.getXmlContentFromString(content);
        FileDataHolder fileDataHolder = XmlContentParser.getFileDataFromXmlContent(xmlContent);
        parent.setFileDataHolder(fileDataHolder);
        parent.setState(ReaderStateEnum.VALIDATING_STATE);
        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateParseXml");
    }
}
