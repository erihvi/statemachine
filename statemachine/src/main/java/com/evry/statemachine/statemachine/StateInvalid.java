package com.evry.statemachine.statemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateInvalid extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateInvalid.class);

    public StateInvalid(ReaderStateMachine parent) {
        super(ReaderStateEnum.INVALID_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateInvalid");
    }

    @Override
    public void handle() throws Exception {

    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateInvalid");
    }
}
