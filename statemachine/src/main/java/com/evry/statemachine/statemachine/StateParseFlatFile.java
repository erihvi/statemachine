package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.contentparsers.FlatFileContentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateParseFlatFile extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateParseFlatFile.class);

    public StateParseFlatFile(ReaderStateMachine parent) {
        super(ReaderStateEnum.PARSE_FLAT_FILE_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateParseFlatFile");
    }

    @Override
    public void handle() throws Exception {
        String content = parent.getFileContent();
        FileDataHolder fileDataHolder = FlatFileContentParser.getParsedContent(content);
        parent.setFileDataHolder(fileDataHolder);
        parent.setState(ReaderStateEnum.VALIDATING_STATE);
        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateParseFlatFile");
    }
}
